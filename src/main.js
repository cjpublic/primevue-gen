// import { createApp } from "vue/dist/vue.esm-bundler.js"
import Vue from "vue"
import PrimeVue from "primevue/config"
import 'primevue/resources/themes/lara-light-teal/theme.css'

import Button from "primevue/button"
import Accordion from 'primevue/accordion';
import AccordionTab from 'primevue/accordiontab';

window.createPrimeVueApp = function(opt) {
  const app = Vue.createApp(opt)
  app.use(PrimeVue)

  app.component("p-button", Button)
  app.component("p-accordion", Accordion)
  app.component("p-accordion-tab", AccordionTab)

  return app
}
