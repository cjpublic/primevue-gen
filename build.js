const esbuild = require("esbuild")

const myPlugin = {
  name: "my-plugin",
  setup(build) {
    build.onResolve({ filter: /^vue$/ }, (args) => ({
      path: args.path,
      namespace: "my-plugin",
    }))

    build.onLoad({ filter: /.*/, namespace: "my-plugin" }, () => {
      const contents = `module.exports = Vue`
      return { contents }
    })
  },
}

;(async () => {
  await esbuild.build({
    entryPoints: ["./src/main.js"],
    bundle: true,
    outdir: "lib",
    logLevel: "info",
    loader: {
      ".woff2": "dataurl",
    },
    plugins: [myPlugin],
  })
})()
