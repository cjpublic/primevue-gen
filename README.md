# Primevue Gen

将 PrimeVue 和 Vue3 打包在一起，生成一个 package，用于在 HTML 中使用 PrimeVue。

## Setup

改完 `src/main.js` 以后，运行 `pnpm build` 生成新的资源文件。

打开 `test.html` 测试新的资源文件。

如果一切没问题，拷贝 `lib/primevue+vue.css` 和 `lib/primevue+vue.js` 文件到新项目中使用即可。
